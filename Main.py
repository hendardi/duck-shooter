# Basic OBJ file viewer. needs objloader from:
#  http://www.pygame.org/wiki/OBJFileLoader
# LMB + move: rotate
# RMB + move: pan
# Scroll wheel: zoom in/out
import array
import pyautogui
import pygame
import numpy
import random
import sys
from math import sin, cos, radians
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from pygame.constants import *
from pygame.locals import *

# IMPORT OBJECT LOADER
from objloader import *

# IMPORT SOUND LOADER
from pygame import mixer


def drawText(text, x, y, z, size):
    font = pygame.font.SysFont("consolas", size)
    textSurface = font.render(text, True, (255, 0, 0, 0), (0, 0, 0, 0))
    textData = pygame.image.tostring(textSurface, "RGBA", True)
    glRasterPos3d(x, y, z)
    glDrawPixels(textSurface.get_width(), textSurface.get_height(),
                 GL_RGBA, GL_UNSIGNED_BYTE, textData)


pygame.init()
viewport = (800, 600)
width, height = viewport
hx = viewport[0]/2
hy = viewport[1]/2
srf = pygame.display.set_mode(viewport, OPENGL | DOUBLEBUF)

glLightfv(GL_LIGHT0, GL_POSITION,  (-40, 200, 100, 0.0))
glLightfv(GL_LIGHT0, GL_AMBIENT, (0.2, 0.2, 0.2, 1.0))
glLightfv(GL_LIGHT0, GL_DIFFUSE, (0.5, 0.5, 0.5, 1.0))
glEnable(GL_LIGHT0)
glEnable(GL_LIGHTING)
glEnable(GL_COLOR_MATERIAL)
glEnable(GL_DEPTH_TEST)
glShadeModel(GL_SMOOTH)           # most obj files expect to be smooth-shaded
glMatrixMode(GL_PROJECTION)

glLoadIdentity()
gluPerspective(90.0, width/float(height), 0.1, 150.0)
glMatrixMode(GL_MODELVIEW)
glLoadIdentity()
clock = pygame.time.Clock()

# VARIABLES
totalDuck = 1
list_duck = []
w = False
a = False
s = False
d = False
achievment = True
shoot = False
time = 45
time_achievment = 0
time_show_achievment = 0
score = 0
CTR = 0
CTR_achievment = 0
CTR_show_achievment = 0
combo = 0
num_duck_killed = 0
num_duck_killed_achievment = 0
index = random.randint(0, totalDuck - 1)
index_angka = 0
index_box_location = 0
location_x, location_y, location_z = (0.0, 8.0, 0.0)
lookat_x, lookat_y, lookat_z = (0.0, 8.0, 5.0)
lookup_x, lookup_y, lookup_z = (0.0, 1.0, 0.0)
angleX = 0.0
angleY = 0.0
location_bullet_x, location_bullet_y, location_bullet_z = (0, 0, 0)
location_achievment_x, location_achievment_y, location_achievment_z = (
    -5, 0, 2)

# MUSIC
mixer.init()
mixer.music.load('achievement.wav')

if index_box_location == 0:
    location_box_x, location_box_y, location_box_z = (-4.0, 1.0, -14.0)
    index_box_location = 1
else:
    location_box_x, location_box_y, location_box_z = (16.0, 0.0, 44.0)
    index_box_location = 0

# LOAD HIGHSCORE
f = open("highscore.txt", "r")
high_score = f.read()

# LOAD OBJECT AFTER PYGAME INIT
terrain = Terrain("terrain.obj", swapyz=True)
world = World("world.obj", swapyz=True)
box1 = Box("Crate1.obj", swapyz=True)

for i in range(0, totalDuck):
    duck = Duck("duck.obj", swapyz=True)
    list_duck.append(duck)

while True:
    clock.tick(60)
    for e in pygame.event.get():
        if e.type == MOUSEMOTION:
            i, j = e.rel
            if i > 0:
                angleX += 0.06
                lookat_x = float(sin(angleX)) + float(location_x)
            else:
                angleX -= 0.06
                lookat_x = float(sin(angleX)) + float(location_x)

            if j > 0 and lookat_y > location_y - 0.9:
                angleY += 0.06
                lookat_y = float(-cos(angleY)) + float(location_y)
            elif j < 0 and lookat_y < location_y + 0.9:
                angleY -= 0.06
                lookat_y = float(-cos(angleY)) + float(location_y)

            lookat_z = float(-cos(angleX)) + float(location_z)
            location_bullet_y = lookat_y

        if e.type == pygame.KEYDOWN:
            if e.key == pygame.K_w:
                w = True
            if e.key == pygame.K_a:
                a = True
            if e.key == pygame.K_s:
                s = True
            if e.key == pygame.K_d:
                d = True
            if e.key == pygame.K_ESCAPE and time <= 0:
                if score > int(high_score):
                    f = open("highscore.txt", "w")
                    f.write(str(score))
                    f.close()
                    f = open("highscore.txt", "r")
                    high_score = f.read()
                achievment = True
                shoot = False
                time = 45
                time_achievment = 0
                time_show_achievment = 0
                score = 0
                CTR = 0
                CTR_achievment = 0
                CTR_show_achievment = 0
                combo = 0
                num_duck_killed = 0
                num_duck_killed_achievment = 0
                location_x, location_y, location_z = (0.0, 8.0, 0.0)
                lookat_x, lookat_y, lookat_z = (0.0, 8.0, 5.0)
                lookup_x, lookup_y, lookup_z = (0.0, 1.0, 0.0)
                angleX = 0.0
                angleY = 0.0
        if e.type == pygame.KEYUP:
            if e.key == pygame.K_w:
                w = False
            if e.key == pygame.K_a:
                a = False
            if e.key == pygame.K_s:
                s = False
            if e.key == pygame.K_d:
                d = False

        if e.type == MOUSEBUTTONDOWN:
            if e.button == 3:
                shoot = True
                hit = False

    if time > 0:
        if w == True:
            location_x += float(sin(angleX) * 1)
            lookat_x += float(sin(angleX) * 1)
            location_z -= float(cos(angleX) * 1)
            lookat_z -= float(cos(angleX) * 1)
        if a == True:
            location_x -= float(cos(angleX) * 1)
            lookat_x -= float(cos(angleX) * 1)
            location_z -= float(sin(angleX) * 1)
            lookat_z -= float(sin(angleX) * 1)
        if s == True:
            location_x -= float(sin(angleX) * 1)
            lookat_x -= float(sin(angleX) * 1)
            location_z += float(cos(angleX) * 1)
            lookat_z += float(cos(angleX) * 1)
        if d == True:
            location_x += float(cos(angleX) * 1)
            lookat_x += float(cos(angleX) * 1)
            location_z += float(sin(angleX) * 1)
            lookat_z += float(sin(angleX) * 1)

        location_bullet_x = location_x
        location_bullet_y = location_y
        location_bullet_z = location_z

        # POWER UP COLLISION CHECKING
        if ((location_x > location_box_x - 1.1 and location_x < location_box_x + 1.1) and (location_z > location_box_z - 1.1 and location_z < location_box_z + 1.1)):
            index_angka = random.randint(0, 1)
            if index_angka == 1:
                score += 500
            else:
                score *= 2

            if index_box_location == 0:
                location_box_x, location_box_y, location_box_z = (
                    -4.0, 1.0, -14.0)
                index_box_location = 1
            else:
                location_box_x, location_box_y, location_box_z = (
                    16.0, 0.0, 44.0)
                index_box_location = 0

        # SHOOTING CHECK
        if shoot == True:
            for i in range(0, 100):
                location_bullet_x += float(sin(angleX) * 1)
                location_bullet_z -= float(cos(angleX) * 1)
                location_bullet_y += float(-cos(angleY) * 1)
                if ((location_bullet_x > list_duck[index].x-1.5 and location_bullet_x < list_duck[index].x+1.5) and (location_bullet_z > list_duck[index].z-1.5 and location_bullet_z < list_duck[index].z+1.5) and (location_bullet_y > list_duck[index].y - 1.5)):
                    list_duck[index].x = 60
                    index = random.randint(0, totalDuck - 1)
                    num_duck_killed += 1
                    combo += 1
                    score += 50 * combo
                    hit = True

                    if achievment == True:
                        num_duck_killed_achievment += 1

                    break

            shoot = False
            if hit == False:
                combo = 0

        # DUCK MOVEMENT
        list_duck[index].x += list_duck[index].dx
        if list_duck[index].x > 60:
            list_duck[index].dx *= -1
            index = random.randint(0, totalDuck - 1)
        elif list_duck[index].x < -60:
            list_duck[index].dx *= -1
            index = random.randint(0, totalDuck - 1)

        # COUNTDOWN ACHIEVEMENT
        if achievment == True:
            CTR_achievment += 1
            if CTR_achievment % 25 == 0:
                time_achievment += 1
            if time_achievment <= 5 and num_duck_killed_achievment >= 3:
                mixer.music.play()
                achievment = False
                time_show_achievment = 2
                score += 1000
            elif time_achievment > 5:
                time_achievment = 0
                num_duck_killed_achievment = 0

        if achievment == False and time_show_achievment > 0:
            CTR_show_achievment += 1
            if CTR_show_achievment % 25 == 0:
                time_show_achievment -= 1

        if achievment == True:
            location_achievment_x, location_achievment_y, location_achievment_z = (
                list_duck[index].x, list_duck[index].y, list_duck[index].z)

        # COUNTDOWN TIME REMAINING
        CTR += 1
        if CTR % 25 == 0:
            time -= 1
            CTR = 0

    glLoadIdentity()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    # CAMERA
    gluLookAt(location_x, location_y, location_z, lookat_x,
              lookat_y, lookat_z, lookup_x, lookup_y,  lookup_z)

    drawText("TOTAL DUCK KILLED : " + str(num_duck_killed), 0, 25, 5, 21)
    drawText("TIME REMAINING : " + str(time), 0, 22, 5, 21)
    drawText("SCORE : " + str(score), 0, 19, 5, 21)
    drawText("COMBO : " + str(combo), 0, 16, 5, 21)
    if time_show_achievment > 0:
        drawText("KILLED 3 OR MORE DUCK IN 5 SECONDS", location_achievment_x,
                 location_achievment_y, location_achievment_z, 21)

    # drawText("+", lookat_x, lookat_y, lookat_z, 33) Crosshair
    crossHair = [[lookat_x, lookat_y - 0.01, lookat_z], [lookat_x, lookat_y + 0.01, lookat_z],
                 [lookat_x - 0.01, lookat_y, lookat_z], [lookat_x + 0.01, lookat_y, lookat_z]]

    glBegin(GL_LINES)
    glColor3f(1, 0, 0)
    for i in range(4):
        glVertex3f(crossHair[i][0], crossHair[i][1], crossHair[i][2])
    glEnd()
    glColor3f(1, 1, 1)

    # RENDER DUCK
    for i in range(0, len(list_duck)):
        glPushMatrix()
        glTranslate(list_duck[i].x, list_duck[i].y, list_duck[i].z)
        glCallList(list_duck[i].gl_list)
        glPopMatrix()

    # RENDER BOX SCORE
    glPushMatrix()
    glTranslate(location_box_x, location_box_y, location_box_z)
    glCallList(box1.gl_list)
    glPopMatrix()

    # RENDER TERRAIN
    glCallList(terrain.gl_list)

    # RENDER WORLD
    glCallList(world.gl_list)

    pygame.display.flip()
